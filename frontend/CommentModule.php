<?php

namespace salestools\comments\frontend;

use salestools\comments\common\components\BaseCommentModule;

class CommentModule extends BaseCommentModule
{
    public $controllerNamespace = 'salestools\comments\frontend\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
