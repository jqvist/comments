<?php

namespace salestools\comments\frontend\widgets;

use yii\web\AssetBundle;

/**
 * CommentsAsset
 */
class CommentsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/salestools/comments/frontend/widgets/assets';
    public $js = [
        'js/comments.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}