<?php

namespace salestools\comments\backend\assets;

use yii\web\AssetBundle;

class CommentsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/salestools/comments/backend/assets/src';
    public $js = [
        'comments.js',
    ];
    public $css = [
        'comments.css',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
