<?php

namespace salestools\comments\common\components;

class BaseCommentModule extends \yii\base\Module
{
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}